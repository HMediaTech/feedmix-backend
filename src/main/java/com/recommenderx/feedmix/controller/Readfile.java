package com.recommenderx.feedmix.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Readfile {
	private Scanner x;
	private List<String> username = new ArrayList<String>();
	private List<String> hashtag = new ArrayList<String>();
	private List<String> subreddit = new ArrayList<String>();
	private List<String> redditquery = new ArrayList<String>();

	public void openFile() {
		try {
			x = new Scanner(new File("ReadFile.txt"));
		} catch (Exception e) {
			System.out.println("not found");
		}
	}

	public void readFile() {
		while (x.hasNext()) {
			String element = x.next();
			if (element.contains("@")) {
				username.add(element.substring(1));
			} else if (element.contains("#")) {
				hashtag.add(element.substring(1));
			} else if(element.contains("&")){
				subreddit.add(element.substring(1));
			} else if(element.contains("%")){
				redditquery.add(element.substring(1));
			}
		}
	}

	public void closeFile() {
		x.close();
	}
    
	//file reading methods inside one method
	public void fileReading(){
		openFile();
		readFile();
		closeFile();
	}
	public List<String> getHashtag() {
		return hashtag;
	}

	public void setHashtag(List<String> hashtag) {
		this.hashtag = hashtag;
	}

	public List<String> getUsername() {
		return username;
	}

	public void setUsername(List<String> username) {
		this.username = username;
	}
	
	public List<String> getSubreddit() {
		return subreddit;
	}

	public void setSubreddit(List<String> subreddit) {
		this.subreddit = subreddit;
	}
    
	public List<String> getRedditQuery() {
		return redditquery;
	}

	public void setRedditQuery(List<String> redditquery) {
		this.redditquery = redditquery;
	}

}
