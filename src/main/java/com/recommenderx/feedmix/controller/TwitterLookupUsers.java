package com.recommenderx.feedmix.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import twitter4j.ResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

public class TwitterLookupUsers {

	public List<Long> getuserIDS(List<String> names) throws TwitterException, TimeoutException {

		// initializing new ArrayList for id(s).
		final List<Long> ids = new ArrayList<Long>();

		// twitter oauth configuration
		Twitter twitter = new TwitterFactory(ConfigurationBuilderingTwitter
				.getConfigurationBuilder().build())
				.getInstance();

		// getting user names to ResponseList
		ResponseList<User> users = twitter.lookupUsers(names.toArray(new String[names.size()]));

		for (User user : users) {
			// checking if the user is private
			if (user.getStatus() != null) {

				// copying user name(s)' id(s) to id(s)
				ids.add(user.getId());

			} else {

				// the user is protected
				System.out.println("this users account is protected");
			}
		}
		return ids;
	}
}
