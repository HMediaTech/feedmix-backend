package com.recommenderx.feedmix.controller;

import java.util.Date;

import org.bson.Document;

import com.google.gson.Gson;

public class FeedItem {

	private String title;
	private String description;
	private String image;
	private Date published;
	private String author;
	private String source;
	private String url;
	private String video;
	private String avatar;
	private Integer score;
	private Boolean media;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Date getPublished() {
		return published;
	}

	public void setPublished(Date published) {
		this.published = published;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}
    
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
	
	public Boolean getMedia() {
		return media;
	}

	public void setMedia(Boolean media) {
		this.media = media;
	}
	
	public static FeedItem fromDocument(Document doc) {
		FeedItem fi = new FeedItem();
		fi.setAuthor(doc.getString("author"));
		fi.setPublished(doc.getDate("published"));
		fi.setAvatar(doc.getString("avatar"));
		fi.setImage(doc.getString("image"));
		fi.setMedia(doc.getBoolean("media"));
		fi.setScore(doc.getInteger("score"));
		fi.setSource(doc.getString("source"));
		fi.setDescription(doc.getString("description"));
	    fi.setTitle(doc.getString("title"));
	    fi.setUrl(doc.getString("url"));
	    fi.setVideo(doc.getString("video"));
		return fi;
	}
	
	public Document toDocument() {
		Gson gson = new Gson();
		// Convert strings to json
		String info = gson.toJson(this);
		Document doc = Document.parse(info);
		doc.put("published", getPublished());
		return doc;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FeedItem) {
			return ((FeedItem) obj).getUrl().equals(getUrl());
		} else {
			return false;
		}
	}

}
