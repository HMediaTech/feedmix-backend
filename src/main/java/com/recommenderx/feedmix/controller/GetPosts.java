package com.recommenderx.feedmix.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mongodb.BasicDBObject;

public class GetPosts {

	 BufferedWriter bw;
	 int postCounts = 0;
    
	public List<BasicDBObject> filterList(String filterStep, String source){
		//first filtering list
		List<BasicDBObject> firstFilter = new ArrayList<BasicDBObject>();
 		firstFilter.add(new BasicDBObject("media",true)
 		.append("source", source)
 		.append("score", 100));
 		
 		//second filtering list
		 List<BasicDBObject> secondFilter = new ArrayList<BasicDBObject>();
         secondFilter.add(new BasicDBObject("score", 
         new BasicDBObject("$gte", 70)
         .append("$lte", 100)).append("source", source));
         
         //third filtering
		 List<BasicDBObject> thirdFilter = new ArrayList<BasicDBObject>();
		 thirdFilter.add(new BasicDBObject("media", true)
				 .append("source", source));
		 
		 //fourth filtering
		 List<BasicDBObject> fourthFilter = new ArrayList<BasicDBObject>();
		 fourthFilter.add(new BasicDBObject("source",source));
		 
		 switch (filterStep){
			 case "first":
				 return firstFilter;
			 case "second":
				 return secondFilter;
			 case "third":
				 return thirdFilter;
			 case "fourth":
				 return fourthFilter;
			 default:
				 return null;
		 } 
			 
	}
	
	public void Posts() throws IOException {
        
	    int twitterCount = 0;
		int redditCount = 0;
		
		List<FeedItem> twitterList = new ArrayList<FeedItem>();
	    List<FeedItem> redditList = new ArrayList<FeedItem>();
	    List<FeedItem> rssList = new ArrayList<FeedItem>();
	    List<FeedItem> interfaceList = new ArrayList<FeedItem>();

		Filtration fl = new Filtration();
		
		//first filtering
		twitterList.addAll(fl.Filtering(10, filterList("first", "Twitter")));
		redditList.addAll(fl.Filtering(5, filterList("first", "Reddit")));
				
		interfaceList.addAll(twitterList);
		interfaceList.addAll(redditList);
        
		twitterCount = 10 - twitterList.size();
		redditCount = 5 - redditList.size();
		
		// second filtering
		if (twitterList.size() < 10) {
			
			List<FeedItem> tempArray = fl.Filtering(twitterCount, filterList("second", "Twitter"));

			for (FeedItem temp : tempArray) {
				if (!twitterList.contains(temp)) {
					twitterList.add(temp);
					interfaceList.add(temp);
				}
			}

			// checking if filtering did not reach the target
			twitterCount = 10 - twitterList.size();
		}
		if (redditList.size() < 5) {
			
			List<FeedItem> tempArray = fl.Filtering(redditCount, filterList("second", "Reddit"));

			for (FeedItem temp : tempArray) {
				if (!redditList.contains(temp)) {
					redditList.add(temp);
					interfaceList.add(temp);
				}
			}
			
			// checking if filtering did not reach the target
			redditCount = 5 - redditList.size();
		}
        
		// third filtering
		if (twitterList.size() < 10) {
			
			List<FeedItem> tempArray = fl.Filtering(twitterCount, filterList("third", "Twitter"));

			for (FeedItem temp : tempArray) {
				if (!twitterList.contains(temp)) {
					twitterList.add(temp);
					interfaceList.add(temp);
				}
			}

			// checking if filtering did not reach the target
			twitterCount = 10 - twitterList.size();
		}
		
		if (redditList.size() < 5) {
		
			List<FeedItem> tempArray = fl.Filtering(redditCount, filterList("third", "Reddit"));

			for (FeedItem temp : tempArray) {
				if (!redditList.contains(temp)) {
					redditList.add(temp);
					interfaceList.add(temp);
				}
			}

			// checking if filtering did not reach the target
			redditCount = 5 - redditList.size();
		}
		
		rssList.addAll(fl.Filtering(5, filterList("third", "RSS")));
		interfaceList.addAll(rssList);
	
		//fourth filtering
        if (twitterList.size() < 10) {
			
			List<FeedItem> tempArray = fl.Filtering(10, filterList("fourth", "Twitter"));
			System.out.println(tempArray.size());
			
			for (FeedItem temp : tempArray) {
				if (!twitterList.contains(temp) && twitterList.size() < 10) {
					twitterList.add(temp);
					interfaceList.add(temp);
				}
			}

		}
		if (redditList.size() < 5) {
		
			List<FeedItem> tempArray = fl.Filtering(5, filterList("fourth", "Reddit"));

			for (FeedItem temp : tempArray) {
				if (!redditList.contains(temp) && redditList.size() < 5) {
					redditList.add(temp);
					interfaceList.add(temp);
				}
			}
		}
		
		if(rssList.size() < 5){
			
			List<FeedItem> tempArray = fl.Filtering(5, filterList("fourth", "RSS"));

			for (FeedItem temp : tempArray) {
				if (!rssList.contains(temp) && rssList.size() < 5) {
					rssList.add(temp);
					interfaceList.add(temp);
				}
			}
		}
		
		//writing to files
		Date date = new Date();
	    SimpleDateFormat df = new SimpleDateFormat("MM-dd-YYYY_HH-mm a");
		String formattedDate = df.format(date);
		
		File file= new File(formattedDate+".txt");
		 
		 if(!file.exists()){
		   try{
			   file.createNewFile();
			}
			catch (IOException e){
				e.printStackTrace();
			}
		   FileWriter fw = new FileWriter(file);
		   bw = new BufferedWriter(fw);
		 }
			
		for (FeedItem item : interfaceList) {
			postCounts++;
			bw.write("Source: "+item.getSource());
			bw.newLine();
			bw.write("Title: "+item.getTitle());
			bw.newLine();
		    bw.write("Text: "+item.getDescription());
		    bw.newLine();
		    bw.write("Author: "+item.getAuthor());
		    bw.newLine();
    	    bw.write("Avatar: "+item.getAvatar());
    	    bw.newLine();
    	    bw.write("Url: "+item.getUrl());
    	    bw.newLine();
    	    bw.write("Score: "+item.getScore());
    	    bw.newLine();

		    if(item.getMedia()){
			    bw.write("Image: "+item.getImage());
			    bw.newLine();
			    bw.write("Video: "+item.getVideo());
			    bw.newLine();
			}
		    bw.write("Published at: "+item.getPublished());
		    bw.newLine();
		    bw.newLine();
		}
		bw.write("Count of posts: "+postCounts);
		
		bw.close();
	}
}
