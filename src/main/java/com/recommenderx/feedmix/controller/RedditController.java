package com.recommenderx.feedmix.controller;

import java.util.List;

import com.mongodb.BasicDBObject;

import net.dean.jraw.RedditClient;
import net.dean.jraw.http.NetworkException;
import net.dean.jraw.http.oauth.OAuthException;
import net.dean.jraw.models.Listing;
import net.dean.jraw.models.Submission;
import net.dean.jraw.paginators.Sorting;
import net.dean.jraw.paginators.SubmissionSearchPaginator;
import net.dean.jraw.paginators.SubredditPaginator;

public class RedditController {

	public void PostController(List<String> sb) throws NetworkException, OAuthException {

		// reddit configuration
		String table = "Common";
		RedditClient redditclient = ConfigurationBuilderReddit.redditConfig();

		BasicDBObject whereQuery = new BasicDBObject();

		// Converting the submission list to array
		final String subredditarray[] = sb.toArray(new String[sb.size()]);

		for (int i = 0; i < subredditarray.length; i++) {
			SubredditPaginator sp = new SubredditPaginator(redditclient, subredditarray[i]);
			sp.setSorting(Sorting.HOT);

			Listing<Submission> subs = sp.next();

			for (Submission sub : subs) {

				// get the submission link
				String link = "www.reddit.com" + sub.getPermalink();

				// put it to the query
				whereQuery.put("url", link);

				// connecting to mongodb
				MongoDBConfiguration mc = new MongoDBConfiguration();
				long count = mc.connectMongo(table).count(whereQuery);
				mc.closeConnection();

				// check if
				if (count == 0) {
					// insert mongo db
					DatabaseRedditInsertion(sub);
				}
			}
		}

	}

	// method for querying
	public void PostQuery(List<String> qu) throws NetworkException, OAuthException {
		// reddit configuration
		RedditClient redditclient = ConfigurationBuilderReddit.redditConfig();

		final String queryArray[] = qu.toArray(new String[qu.size()]);

		for (int i = 0; i < queryArray.length; i++) {

			SubmissionSearchPaginator ssp = new SubmissionSearchPaginator(redditclient, queryArray[i]);
			Listing<Submission> posts = ssp.next();
			for (Submission post : posts) {
				// insertion mongo db
				DatabaseRedditInsertion(post);
			}
		}

	}

	public FeedItem setRedditItem(Submission submission) {
		FeedItem ri = new FeedItem();
		ri.setTitle(submission.getTitle());
		ri.setAuthor(submission.getAuthor());
		ri.setSource("Reddit");
		ri.setUrl("www.reddit.com" + submission.getPermalink());
		ri.setPublished(submission.getCreated());
		if (submission.getThumbnail() != null) {
			ri.setImage(submission.getThumbnail());
		}
		if (submission.getOEmbedMedia() != null) {
			ri.setVideo(submission.getUrl());
		}
		if (submission.getThumbnail() != null || submission.getOEmbedMedia() != null){
			ri.setMedia(true);
		}
		else
			ri.setMedia(false);
		
		if (submission.getScore()>=10000)
			ri.setScore(100);
		else if(submission.getScore()>=9000 && submission.getScore()<10000)
			ri.setScore(90);
		else if (submission.getScore()>=8000 && submission.getScore()<9000)
			ri.setScore(800);
		else if(submission.getScore()>=7000 && submission.getScore()<8000)
			ri.setScore(70);
		else if(submission.getScore()>=6000 && submission.getScore()<7000)
			ri.setScore(60);
		else if(submission.getScore()>=5000 && submission.getScore()<6000)
			ri.setScore(50);
		else if(submission.getScore()<5000)
			ri.setScore(40);
		
		return ri;
	}

	// database insertion methods
	public void DatabaseRedditInsertion(Submission a) {
		FeedItem b = setRedditItem(a);
		new Database_Controller().insertCommon(b);
		new Database_Controller().insertReddit(a);
	}
}
