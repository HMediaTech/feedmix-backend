package com.recommenderx.feedmix;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.recommenderx.feedmix.controller.GetPosts;
import com.recommenderx.feedmix.controller.Readfile;
import com.recommenderx.feedmix.controller.RedditController;
import com.recommenderx.feedmix.controller.TwitterController;
import com.recommenderx.feedmix.controller.TwitterLookupUsers;

import net.dean.jraw.http.NetworkException;
import net.dean.jraw.http.oauth.OAuthException;
import twitter4j.TwitterException;

public class App {
    
	public static void main(String[] args) throws TwitterException, TimeoutException, NetworkException, OAuthException, IOException {
		 startTwitterListener();
	}
	
	public static void startFiltering() throws IOException{
		 GetPosts gp = new GetPosts();
	     gp.Posts();
	}
	
	public static void startRedditListener() throws NetworkException, OAuthException {
		List<String> subs;
		List<String> query;

		Readfile rf = new Readfile();
		rf.fileReading();
		subs = rf.getSubreddit();
		query = rf.getRedditQuery();

		if (subs != null && subs.size() > 0) {
			new RedditController().PostController(subs);
		}
		if (query != null && query.size() > 0) {
			new RedditController().PostQuery(query);
		}
	}

	public static void startTwitterListener() throws TwitterException, TimeoutException {
		List<String> hash;
		List<String> unames;
		List<Long> id = new ArrayList<Long>();

		// 1. read files to get list of hashtags and users
		Readfile rf = new Readfile();
		rf.fileReading();
		hash = rf.getHashtag();
		unames = rf.getUsername();

		// 2. convert user names to ids
		if (!unames.isEmpty()) {
			TwitterLookupUsers lu = new TwitterLookupUsers();
			id = lu.getuserIDS(unames);
		}

		// 3. pass list of tags and names to twitter and start listening

		TwitterController tc = new TwitterController();
		tc.filterTwitter(hash, id);

	}
}
