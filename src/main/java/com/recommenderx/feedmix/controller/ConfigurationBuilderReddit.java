package com.recommenderx.feedmix.controller;

import net.dean.jraw.RedditClient;
import net.dean.jraw.http.NetworkException;
import net.dean.jraw.http.UserAgent;
import net.dean.jraw.http.oauth.Credentials;
import net.dean.jraw.http.oauth.OAuthData;
import net.dean.jraw.http.oauth.OAuthException;

public class ConfigurationBuilderReddit {

	public static RedditClient redditConfig() throws NetworkException, OAuthException{
		UserAgent myUserAgent = UserAgent.of("desktop", "com.recommenderx.feedmix", "v0.1", "recommenderx");
		RedditClient redditClient = new RedditClient(myUserAgent);
		Credentials credentials = Credentials.script("recommenderx", "recommenderx", "_VJqECXJ4-npGg", "t-fS4CraC5nyB72qIMBfimYgHOM");
		OAuthData authData = redditClient.getOAuthHelper().easyAuth(credentials);
		redditClient.authenticate(authData);
		return redditClient;
	}
}
