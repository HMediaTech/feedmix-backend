package com.recommenderx.feedmix.controller;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.joda.time.DateTime;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;

public class Filtration {

	public int countPost, redditCount, twitterCount;
	private String table = "Common";

	
	public List<FeedItem> Filtering( Integer count, List<BasicDBObject> list)
	{
		List<FeedItem> result =new ArrayList<FeedItem>();
        BasicDBObject posts = new BasicDBObject();
	
		posts.put("$and", list);
		
		// connecting to mongodb
		MongoDBConfiguration mc = new MongoDBConfiguration();
		
		FindIterable<Document> cursor  = mc.connectMongo(table)
				.find(posts
				.append("published", new BasicDBObject("$gte", new DateTime().minusHours(24).toDate())))
				.sort(new BasicDBObject("score", -1))
				        .limit(count);
		
		for(Document docs : cursor){
			FeedItem fm = FeedItem.fromDocument(docs);
			result.add(fm);
		}	
	
		mc.closeConnection();
		
		return result;
		
	}
}
