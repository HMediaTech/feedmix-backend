package com.recommenderx.feedmix.controller;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBConfiguration {
	
//	static Logger root = (Logger) LoggerFactory
//	        .getLogger(Logger.ROOT_LOGGER_NAME);
//
//	static {
//	    root.setLevel(Level.INFO);
//	}

	private MongoClient mongo;

	public MongoCollection<Document> connectMongo(String table_name) {

		// connecting to mongodb
		mongo = new MongoClient("localhost", 27017);
		MongoDatabase db = mongo.getDatabase("FeedMix");
		MongoCollection<Document> source_table = db.getCollection(table_name);

		return source_table;
	}

	public void closeConnection() {
		if (mongo != null) {
			mongo.close();
			mongo = null;
		}
	}
	
	

}
