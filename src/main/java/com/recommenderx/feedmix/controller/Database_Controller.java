package com.recommenderx.feedmix.controller;

//import files for mongo db
import org.bson.Document;

import com.google.gson.Gson;

import net.dean.jraw.models.Submission;
import twitter4j.Status;

public class Database_Controller {

	// insert tweets to Twitter collection
	public void insertTweet(Status status) {
		insertionMongo(status, "Twitter");
	}

	// insert tweets to Common collection
	public void insertCommon(FeedItem fi) {

		MongoDBConfiguration mc = new MongoDBConfiguration();

		mc.connectMongo("Common").insertOne(fi.toDocument());

		// close db
		mc.closeConnection();
	}

	// insert reddit posts to Reddit collection
	public void insertReddit(Submission sb) {
		insertionMongo(sb, "Reddit");
	}

	public void insertionMongo(Object obj, String table) {

		MongoDBConfiguration mc = new MongoDBConfiguration();

		// Inserting tweets to database
		Gson gson = new Gson();

		// Convert strings to json
		String info = gson.toJson(obj);
		mc.connectMongo(table).insertOne(Document.parse(info));

		// close db
		mc.closeConnection();
	}
}
