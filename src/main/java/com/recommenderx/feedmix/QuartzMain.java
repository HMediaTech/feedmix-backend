package com.recommenderx.feedmix;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzMain {
	public static void main(String[] args) throws SchedulerException {
		
		JobKey jobkeyReddit = new JobKey("jobReddit", "group1");
		JobDetail jobReddit = JobBuilder.newJob(QuartzJobReddit.class)
				.withIdentity(jobkeyReddit).build();
		
		JobKey jobkeyFiltering = new JobKey("jobFiltering", "group1");
		JobDetail jobFiltering = JobBuilder.newJob(QuartzJobFiltering.class)
				.withIdentity(jobkeyFiltering).build();
		
		Trigger triggerReddit = TriggerBuilder.newTrigger().withIdentity("RedditTrigger", "group1")
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(02)
						.repeatForever()).build();
		
		Trigger triggerFiltering = TriggerBuilder.newTrigger().withIdentity("FilteringTrigger", "group1")
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(15)
						.repeatForever()).build();
		
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		
		scheduler.start();
		scheduler.scheduleJob(jobReddit, triggerReddit);
		scheduler.scheduleJob(jobFiltering, triggerFiltering);
	}
}
