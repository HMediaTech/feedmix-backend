package com.recommenderx.feedmix.controller;

import twitter4j.MediaEntity;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

public class TwitterListener implements StatusListener {
			
	private long[] myArray;
	

	public TwitterListener(long[] array) {
		myArray = array;
	}

	// set tweet info to appropriate variables
	public FeedItem setFeedTweet(Status status) {
        boolean verify=status.getUser().isVerified();
		FeedItem fi = new FeedItem();
		fi.setDescription(status.getText());
		fi.setPublished(status.getCreatedAt());
		fi.setAuthor(status.getUser().getName() + " (@" + status.getUser().getScreenName() + ")");
		fi.setUrl("https://twitter.com/" + status.getUser().getScreenName() + "/status/" + status.getId());
		fi.setAvatar(status.getUser().getProfileImageURL());
		fi.setSource("Twitter");
		if(verify)
			fi.setScore(100);
		else
			fi.setScore(40);

		// getting image url and video url
		MediaEntity[] mediaUrls = status.getMediaEntities();
		if (mediaUrls!=null && mediaUrls.length > 0 ){
			//set media field true
			fi.setMedia(true);
			MediaEntity myEntity = mediaUrls[0];
			String type = myEntity.getType();
			if (type.equals("photo")) {
				System.out.println(myEntity.getMediaURL() + " being set as the image URL");
				fi.setImage(myEntity.getMediaURL());
			} else if (type.equals("video")) {
				int min = Integer.MAX_VALUE;
				int cursor;
				for (int i = 0; i < myEntity.getVideoVariants().length; i++) {

					int bitrate = myEntity.getVideoVariants()[i].getBitrate();

					if (bitrate != 0 && bitrate < min) {
						min = bitrate;
						cursor = i;
						fi.setVideo(myEntity.getVideoVariants()[cursor].getUrl());
					}
				}
			}
		}
		else 
			fi.setMedia(false);
		return fi;

	}

	// database insertion methods
	public void DatabaseTwitterInsertion(Status a) {
		FeedItem b = setFeedTweet(a);
		new Database_Controller().insertTweet(a);
		new Database_Controller().insertCommon(b);
	}

	@Override
	public void onException(Exception ex) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatus(Status status) {
		// TODO Auto-generated method stub
	
		for (int i = 0; i < myArray.length; ++i) {
			if (status.getUser().getId() == myArray[i]) {

				// insert tweets to DB
				DatabaseTwitterInsertion(status);

			}
		}
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScrubGeo(long userId, long upToStatusId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStallWarning(StallWarning warning) {
		// TODO Auto-generated method stub

	}

}
