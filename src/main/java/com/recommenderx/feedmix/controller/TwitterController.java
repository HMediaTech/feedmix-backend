package com.recommenderx.feedmix.controller;

import java.util.List;
import java.util.concurrent.TimeoutException;

import twitter4j.FilterQuery;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

public class TwitterController {

	public void filterTwitter(List<String> hashlist, List<Long> userlist) throws TimeoutException, TwitterException {

		// Initiating twitter stream using authentication
		TwitterStream twitterStream = new TwitterStreamFactory(
				ConfigurationBuilderingTwitter.getConfigurationBuilder().build()).getInstance();

		// Convert id(s) list to unameArray[]
		final Long unameArray[] = userlist.toArray(new Long[userlist.size()]);

		// converting the hashtag(s) list to hashArray
		final String hashArray[] = hashlist.toArray(new String[hashlist.size()]);

		// copying tempArray elements to arr
		final long[] arr = new long[unameArray.length];

		/*
		 * converting (Long) unameArray elements to (long) arr array because,
		 * filter query requires (long) type array elements
		 */
		for (int i = 0; i < arr.length; ++i) {
			arr[i] = unameArray[i].longValue();
		}

		// instantiate of Listener class object
		TwitterListener ls = new TwitterListener(arr);

		// Adding listener to twitter stream
		twitterStream.addListener(ls);

		// listening array of elements from twitter stream
		FilterQuery statusFiltering = new FilterQuery();

		if (hashArray.length != 0) {
			statusFiltering.track(hashArray);
		}

		if (arr.length != 0) {
			statusFiltering.follow(arr);
		}
		twitterStream.filter(statusFiltering);

	}

}
